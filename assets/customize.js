$(function () {
    var data = {};
    data.design1 = {};
    data.design1.color = "#fd8134";
    data.design2 = {};
    data.design2.color = "#56c596";

    var strData = localStorage.getItem("data-customize-logo");
    if (strData != null) {
        data = JSON.parse(strData);
        if (data.design1 == null) data.design1 = {};
        if (data.design2 == null) data.design2 = {};
    }

    var uploadedLogo = sessionStorage.getItem("enquiry-logo");
    if (uploadedLogo != null) {
        data.logo = uploadedLogo;
        sessionStorage.removeItem("enquiry-logo");
    }

    var email = sessionStorage.getItem("enquiry-email");
    if (email != null) {
        data.email = email;
        sessionStorage.removeItem("enquiry-email");
    }

    localStorage.setItem("data-customize-logo", JSON.stringify(data));

    if (data.logo != undefined)
        $(".logo__img").prop("src", data.logo);

    if (window.location.search.indexOf("thank=yes") > -1)
        $(".customize__intro-subtitle").show();

    $(".customize__intro-footer button").on("click", function () {
        $(".customize__intro").hide();
        $(".customize__wizard").show();
        $("html, body").animate({ scrollTop: 300 }, "slow");
    });

    $("#link-logo").on("click", function (e) {
        $("#upload-logo").click();
    });

    $("#upload-logo").on('change', function (e) {
        var reader = new FileReader();
        reader.readAsDataURL(e.target.files[0]);

        reader.onload = function () {
            $(".logo__img").prop("src", reader.result);

            data.logo = reader.result;
            localStorage.setItem("data-customize-logo", JSON.stringify(data));

            sessionStorage.removeItem("institutional-logo");
        }
    });

    $(".customize-image__colors li").each(function () {
        var parent = $(this).parents(".row");
        var sectionName = parent.attr("data-section-name");

        var color = $(this).attr("data-color").toLowerCase();

        $(this).children("span").css("background-color", color);

        if (data[sectionName].color == color) {
            $(this).addClass("selected");

            $(`.${sectionName}-mask path`).css("fill", color);
        }
    });

    $(".customize-image__colors li span").on("click", function () {
        var parent = $(this).parents(".row");
        var sectionName = parent.attr("data-section-name");

        $(`.${sectionName}-mask path`).css("fill", $(this).css("background-color"));

        parent.find(".customize-image__colors li").removeClass("selected");
        $(".customize-image__colors .color-picker input").removeClass("selected");
        $(this).parent().addClass("selected");

        var rgb = $(this).css("backgroundColor");
        var hex = '#' + rgb.substr(4, rgb.indexOf(')') - 4).split(',').map((color) => parseInt(color))
            .map((color) => (color < 10 ? '0' + color : color.toString(16))).join('');
        data[sectionName].color = hex;

        localStorage.setItem("data-customize-logo", JSON.stringify(data));
    });

    $(".customize-image__colors .color-picker input").on("change", function () {
        var parent = $(this).parents(".row");
        var sectionName = parent.attr("data-section-name");

        $(`.${sectionName}-mask path`).css("fill", $(this).val());

        parent.find(".customize-image__colors li").removeClass("selected");
        $(this).addClass("selected");

        data[sectionName].color = $(this).val();

        localStorage.setItem("data-customize-logo", JSON.stringify(data));
    });

    $(".customize-image__actions a").on("click", function () {
        var action = $(this).attr("data-action");
        var parent = $(this).parents(".row");

        parent.find(".customize-image__container").each(function () {
            $(this).hasClass(action) ? $(this).show() : $(this).hide();
        });
    });

    $(".customize__wizard-nav a").on("click", function () {
        if (!data.logo) return;

        var action = $(this).attr("data-action");

        var stepNumber = parseInt($(".customize__wizard-step:visible").attr("data-step"));
        action == "next" ? stepNumber++ : stepNumber--;

        var target = $(`.customize__wizard-step[data-step='${stepNumber}']`);
        if (target.length > 0) {
            $(`.customize__wizard-step`).hide();

            target.show();
            $("html, body").animate({ scrollTop: 300 }, "slow");
        }

        var totalSteps = $(`.customize__wizard-step`).length;
        $(".customize__wizard-nav a[data-action='back']").css("visibility", stepNumber <= 1 ? 'hidden' : 'visible');
        $(".customize__wizard-nav a[data-action='next']").css("visibility", stepNumber >= totalSteps ? 'hidden' : 'visible');
    });

    $("#emailFlyerModal .loading-modal__close a").on("click", function() {
        $("#emailFlyerModal").removeClass("show");
    });

    $("#emailFlyerModal button").on("click", function () {
        data.confirmedEmail = $("#emailFlyerModal .input-full").val();

        if (data.confirmedEmail) {
            if (!data.email)
                data.email = data.confirmedEmail;
            localStorage.setItem("data-customize-logo", JSON.stringify(data));

            $(".customize-preview__action[data-action='email']").click();
            $("#emailFlyerModal").removeClass("show");
        }
    });

    $(".customize-preview__action").on("click", function () {
        var url = $(this).attr("data-url");
        var action = $(this).attr("data-action");

        $.get(url, function (response) {
            var strData = localStorage.getItem("data-customize-logo");
            if (strData != null) {
                var data = JSON.parse(strData);

                response = response.replace(/\[LOGO_URL\]/g, data.logo);
                if (data.design1 != null)
                    response = response.replace(/\[DESIGN1_COLOR\]/g, data.design1.color);
                if (data.design2 != null)
                    response = response.replace(/\[DESIGN2_COLOR\]/g, data.design2.color);
            }

            if (action == "email" && !data.confirmedEmail) {
                if (data.email)
                    $("#emailFlyerModal .input-full").val(data.email);

                $("#emailFlyerModal").addClass("show");
            }
            else if (action == "download" || (action == "email" && data.confirmedEmail)) {
                $("#loadingFlyerModal").addClass("show");

                var logoType = data.logo.split(';')[0].split('/')[1];
                //var toAddress = "kumar@yectra.com"; 
                var toAddress = "cd@myppe.in,coo@myppe.in,care@myprotection.in";
                var emailText = "";
                if (action == "email") {
                    $("#loadingFlyerModal h4").text("Finalizing your custom mask flyer email");

                    toAddress = `${data.confirmedEmail},${toAddress}`;
                    emailText = `
                        <div style='padding-bottom: 20px;'>Thank you for creating your custom mask flyer.</div>
                        <div style='padding-bottom: 20px;'>Find the flyer and the original logo attached for your reference.</div>
                        <div style='padding-bottom: 20px;'>Below are the color options choosen:</div>
                        <ul>
                            <li>Design 1: ${data.design1.color}</li>
                            <li>Design 2: ${data.design2.color}</li>
                        </ul>
                    `;
                }
                else {
                    $("#loadingFlyerModal h4").text("Finalizing your custom mask flyer");

                    emailText = `
                        <div style='padding-bottom: 20px;'>You have received a new enquiry and they have downloaded customized flyer.</div>
                        <div style='padding-bottom: 20px;'>Find your custom mask flyer attached.</div>
                        <div style='padding-bottom: 20px;'>Below are the color options choosen:</div>
                        <ul>
                            <li>Email: ${data.confirmedEmail}</li>
                            <li>Design 1: ${data.design1.color}</li>
                            <li>Design 2: ${data.design2.color}</li>
                        </ul>
                    `;
                }

                data.confirmedEmail = null;
                localStorage.setItem("data-customize-logo", JSON.stringify(data));

                fetch('https://pdfgeneratoryectra.azurewebsites.net/api/Download?code=FPPpjiqwnMNFiSj11gAUGDRqNUawcjENTU6ofIgIm9bbhS7CAayW5Q==', {
                    method: 'POST',
                    body: JSON.stringify({
                        fileName: 'MY_Mask_Flyer.pdf',
                        html: response,
                        email: {
                            "toAddress": toAddress,
                            "fromAddress": "care@myprotection.in",
                            "subject": "MYMASK Enquiry - Customized Flyer",
                            "body": emailText,
                            "attachment": {
                                "content": data.logo.split(',')[1],
                                "fileName": `logo.${logoType}`,
                                "type": `image/${logoType}`
                            },
                            "disableFooter": true
                        }
                    }),
                    headers: {
                        'Content-Type': 'application/json'
                    },
                })
                    .then(resp => resp.blob())
                    .then(blob => {
                        if (action == "download") {
                            const url = window.URL.createObjectURL(blob);
                            const a = document.createElement('a');
                            a.style.display = 'none';
                            a.href = url;
                            a.download = 'MY-MASK-Flyer.pdf';
                            document.body.appendChild(a);
                            a.click();
                            window.URL.revokeObjectURL(url);
                        }
                    })
                    .catch((error) => console.log(error))
                    .finally(() => $("#loadingFlyerModal").removeClass("show"));
            }
            else if (action == "preview") {
                response = response.replace(/\[BODY_CLASS\]/g, 'preview');

                var x = window.open('', 'flyer_preview', 'width=808,height=842,titlebar=no,menubar=no,toolbar=no,location=no,scrollbars=yes,resizable=no');
                x.document.open().write(response);
            }

        });
    });
});